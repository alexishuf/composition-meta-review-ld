# composition-meta-review-ld #

This repository contains Turtle (RDF) files used to analyze data collected from a meta-review on service composition (and related research), performed in 2017.

### Where is the data ###

The data is in `src/main/resources`.
All files are handcrafted, except for those in `generated/` and some in `input/`, which came from external sources. Generated files are under version control for convenience. Their generation is stable, re-generating them will always produce the same file.

* `ontology.ttl` The domain-specific ontology used to represent works. Note that some external ontolgoies, such as dublin core metadata are used
* `works.ttl` A generated RDF description of all works considered in the meta-review, according to `ontology.ttl` Includes equivalent data to the bibtex files (done using (bibtex2rdf)[1]) 
* `aspects.ttl` A ontology of aspects considered in classifications. It also contains some terms that are not aspects but are used to qualify them
* `generated/terms.ttl` This is a complementary ontology to the aspects, containing general vocabulary extracted during the survey analysis, expressed in SKOS. This file is processed from `inputs/terms-base.ttl` to include materialization of reifications. 
* `classifications.ttl` The actual classifications, extracted from RQ8 in the information sheets of each work.

### Compile instructions ###

The following should be enough:
```{bash}
mvn clean package
```
However, it may be more comfortable to run from an IDE, after importing the project.

### Usage instructions ###

The output is a jar with multiple runnable applications. All applications include a `--help` flag with details for command line options. 

* `GenerateWorksRDF`: Takes as input a CSV of the consolidated sheet (which may require edit so that the first row contains headers), a bibTeX file with all works from the meta-review and outputs RDF with all them according to `ontology.ttl` and the ontologies used by [bibtex2rdf][1].
* `UpdateAdditionalBibliography`: Transforms additional bib files to RDF and writes `resources/generated/bibliography.bib`
* `GenerateAllResources`: Runs all other command-line classes in order to update all resources in `resources/generated`

### External Libraries ###

All dependencies are in the `pom.xml` and will be downloaded on build. One exception is (bibtex2rdf)[1] by Dr. Wolf Siberski, whose jar is provided in a local maven repository for convenience.

[1]: http://www.l3s.de/~siberski/bibtex2rdf/