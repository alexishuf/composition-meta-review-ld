package br.ufsc.inf.lapesd.composition_meta_review.generators;

import bibtex.rdf.bibtex2rdf;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.CMRO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;
import org.jbibtex.*;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenerateWorksRDF {
    @Option(name = "--in-csv", required = true, usage = "Input CSV with of the consolidated " +
            "sheet. Headers are expected at the first row but are ignored (access is positional).")
    private File inFile;

    @Option(name = "--in-bib", required = true, usage = "Input BibTeX file with all entries " +
            "corresponding to those in the CSV. Correlation is done by ID_1 on mendeley-tags")
    private File inBib;

    @Option(name = "--out", required = true, usage = "Output RDF file. The RDF syntax is " +
            "determined by the file extension.")
    private File outFile;

    @Option(name = "--help", help = true)
    private boolean showHelp = false;

    private static final String WORKS = "http://lapesd.inf.ufsc.br/composition-meta-review-2017/works#";

    private static final Resource[] CLASSES = {
            CMRO.Work, CMRO.Work, CMRO.Work,
            CMRO.BadPublicationTypeWork,
            CMRO.DuplicateWork,
            CMRO.HasExtendedWork,
            CMRO.WorkExcludedByDocument,
            CMRO.WorkExcludedByTitle,
            CMRO.WorkExcludedByAbstract,
            CMRO.WorkWithRQ,
            CMRO.WorkWithRD,
            CMRO.WorkWithOriginal,
            CMRO.OffTopicContentWork,
            CMRO.WorkExcludedByContent,
            CMRO.WorkExcludedByQuality
    };

    private final Pattern id1Rx = Pattern.compile("\\s*(id\\{\\\\_\\}|[RQX])([0-9][0-9]?)\\s*");

    public GenerateWorksRDF() { }

    public GenerateWorksRDF(File inFile, File inBib, File outFile) {
        this.inFile = inFile;
        this.inBib = inBib;
        this.outFile = outFile;
    }

    public static void main(String[] args) throws Exception {
        GenerateWorksRDF app = new GenerateWorksRDF();
        CmdLineParser parser = new CmdLineParser(app);
        try {
            parser.parseArgument(args);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            printUsage(parser);
        }

        if (app.showHelp) printUsage(parser);
        else app.run();
    }

    private static void printUsage(CmdLineParser parser) {
        System.err.printf("Usage:\n");
        parser.printSingleLineUsage(System.err);
        System.err.println();
        parser.printUsage(System.err);
    }

    public void run() throws Exception {
        Model model = convertBibToRDF(inBib);
        model.setNsPrefix("cmro", CMRO.PREFIX)
                .setNsPrefix("works", WORKS)
                .setNsPrefix("rdf", RDF.getURI())
                .setNsPrefix("dct", DCTerms.getURI())
                .setNsPrefix("rdfs", RDFS.getURI())
                .setNsPrefix("xsd", XSD.getURI());

        CSVFormat format = CSVFormat.DEFAULT.withAllowMissingColumnNames().withHeader().withSkipHeaderRecord();
        CSVParser csv = CSVParser.parse(inFile, Charset.forName("UTF-8"), format);
        for (CSVRecord record : csv) {
            String id1 = record.get(0);
            String id2String = record.get(1);
            String id3String = record.get(2);
            Resource r = model.createResource(WORKS + id1).addProperty(RDF.type, CMRO.Work)
                    .addProperty(CMRO.id1, id1);
            if (!id2String.isEmpty()) {
                Literal id2 = ResourceFactory.createTypedLiteral(id2String, XSDDatatype.XSDinteger);
                r.addProperty(CMRO.id2, id2);
            }
            if (!id3String.isEmpty()) {
                Literal id3 = ResourceFactory.createTypedLiteral(id3String, XSDDatatype.XSDinteger);
                r.addProperty(CMRO.id3, id3);
            }

            for (int i = 3; i < CLASSES.length; i++) {
                if (record.get(i).trim().equals("1"))
                    r.addProperty(RDF.type, CLASSES[i]);
            }
        }

        Lang lang = RDFLanguages.filenameToLang(outFile.getName());
        if (lang == null)
            throw new IllegalArgumentException("Couldn't guess format from " + outFile.getName());
        try (FileOutputStream outStream = new FileOutputStream(outFile)) {
            RDFDataMgr.write(outStream, model, lang);
        }
    }

    private Model convertBibToRDF(File inBib) throws IOException, ParseException {
        File file = Files.createTempFile("composition-meta-review", ".ttl").toFile();
        file.deleteOnExit();
        bibtex2rdf.main(new String[] {"-baseURI", WORKS, "-enc", "UTF-8",
                inBib.getAbsolutePath(), file.getAbsolutePath()});
        Model model = ModelFactory.createDefaultModel();
        try (FileInputStream inputStream = new FileInputStream(file)) {
            RDFDataMgr.read(model, inputStream, Lang.RDFXML);
        } finally {
            FileUtils.deleteQuietly(file);
        }

        try (FileReader bibReader = new FileReader(inBib)) {
            BibTeXParser parser = new BibTeXParser();
            BibTeXDatabase db = parser.parseFully(bibReader);
            for (Map.Entry<Key, BibTeXEntry> pair : db.getEntries().entrySet()) {
                String key = pair.getKey().toString();
                Value value = pair.getValue().getField(new Key("mendeley-tags"));
                for (String tag : value.toUserString().split(",")) {
                    Matcher matcher = id1Rx.matcher(tag);
                    if (!matcher.matches()) continue;
                    String localName = matcher.group(2);
                    if (!matcher.group(1).equals("id{\\_}"))
                        localName = matcher.group(1) + localName;
                    renameResource(model, WORKS+key, WORKS+localName);
                }
            }
        }

        return model;
    }

    private void renameResource(Model m, String oldURI, String newURI) {
        Resource source = m.createResource(oldURI);
        Resource target = m.createResource(newURI);
        List<Statement> statements = source.listProperties().toList();
        m.listStatements(null, null, source).forEachRemaining(statements::add);

        for (Statement statement : statements) {
            Resource subject = statement.getSubject();
            if (subject.equals(source)) subject = target;
            Property predicate = statement.getPredicate();
            RDFNode object = statement.getObject();
            if (object.equals(source)) object = target;

            m.remove(statement);
            m.add(subject, predicate, object);
        }
    }
}
