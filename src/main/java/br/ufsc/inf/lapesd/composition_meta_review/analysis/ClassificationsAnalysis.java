package br.ufsc.inf.lapesd.composition_meta_review.analysis;

import br.ufsc.inf.lapesd.composition_meta_review.phrase.*;
import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import br.ufsc.inf.lapesd.composition_meta_review.util.Utils;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.CMRO;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.math.Quantiles;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.inf.lapesd.composition_meta_review.util.Utils.streamObjects;

public class ClassificationsAnalysis {
    @Option(name = "--base-dir", usage = "Optional base project dir, will read from the dir instead of the resource files")
    private File baseDir = null;

    @Option(name = "--out-dir", usage = "Output dir were report files will be written")
    private File out;

    @Option(name = "--dot-cmd", usage = "Alternative dot command")
    private String dotCmd = "dot";

    @Option(name = "--dot-fmt", aliases = {"-T"},
            usage = "Dot output format, e.g. \"png\" for dot's \"-Tpng\"")
    private String[] dotFmts = {"pdf"};

    @Option(name = "--fwd-from", usage = "Draw phrase graphs following forward \"of\" connectives " +
            "from the given phrases")
    private String[] forwardFrom = {};

    @Option(name = "--bwd-from", usage = "Backward version of --fwd-from")
    private String[] backwardFrom = {};

    @Option(name = "--generalizations")
    private boolean generalizations = false;

    @Option(name = "--trim-generalizations", depends = {"--generalizations"})
    private boolean trimGeneralizations = false;

    @Option(name = "--trim-specializations", depends = {"--generalizations"})
    private boolean trimSpecializations = false;

    private Loader loader;
    private boolean querying = false;



    public ClassificationsAnalysis(File baseDir, File out, String dotCmd, String[] dotFmts,
                                   String[] forwardFrom, String[] backwardFrom,
                                   boolean generalizations, boolean trimGeneralizations,
                                   boolean trimSpecializations) {
        this.baseDir = baseDir;
        this.out = out;
        this.dotCmd = dotCmd;
        this.dotFmts = dotFmts;
        this.forwardFrom = forwardFrom;
        this.backwardFrom = backwardFrom;
        this.generalizations = generalizations;
        this.trimGeneralizations = trimGeneralizations;
        this.trimSpecializations = trimSpecializations;
    }

    public static void main(String[] args) throws Exception {
        ClassificationsAnalysis app = new Builder().build();
        new CmdLineParser(app).parseArgument(args);
        app.querying = (app.forwardFrom != null && app.forwardFrom.length > 0)
                || (app.backwardFrom!= null && app.backwardFrom.length > 0);
        app.run();
    }

    public void run() throws IOException {
        loader = new Loader(baseDir);
        Model model = loader.loadModel().withAll(Loader.getAllRDF()).get();
        PhraseFactory pFac = new PhraseFactory();
        List<Resource> works = model.listSubjectsWithProperty(RDF.type, CMRO.Work).toList();
        if (!querying)
            analyzeRoots(model, works, pFac);

        analyzePhraseGraph(model, works, pFac);
    }

    private void analyzePhraseGraph(Model model, List<Resource> works, PhraseFactory pFac)
            throws IOException {
        PhraseGraph graph = generatePhraseGraph(model, works, pFac);

        if (!querying) {
            String base = "phrases" + getGraphTag();
            drawGraph(graph, base, false, e -> true);
            drawGraph(graph, base + "-2", false, e -> e.weight > 1);
            drawGraph(graph, base + "-85perc", false,
                    edgeAbovePercentile(graph, 85));
            drawGraph(graph, base + "-95perc", false,
                    edgeAbovePercentile(graph, 95));
        }

        TextPhraseParser parser = new TextPhraseParser(pFac, loader);
        for (String text : forwardFrom) {
            Phrase phrase = parser.parse(text);
            PhraseGraphClosure closure = PhraseGraphClosure.forward(graph, phrase);
            drawGraph(closure, "fwd-" + getGraphTag() + text, true, e -> true);
        }
        for (String text : backwardFrom) {
            Phrase phrase = parser.parse(text);
            PhraseGraphClosure closure = PhraseGraphClosure.backward(graph, phrase);
            drawGraph(closure, "bwd-" + getGraphTag() + text, false, e -> true);
        }
    }

    private String getGraphTag() {
        return String.format("%s%s%s", generalizations ? "G" : "",
                    trimGeneralizations ? "Tg" : "", trimSpecializations  ? "Ts" : "");
    }

    private Predicate<PhraseGraphImpl.Edge> edgeAbovePercentile(PhraseGraph graph, int percentage) {
        Preconditions.checkArgument(percentage >= 0 && percentage <= 100);
        List<Integer> degrees = graph.getVertices().stream()
                .map(v -> graph.getOutgoingEdges(v).size()).collect(Collectors.toList());
        double threshold = Quantiles.percentiles().index(percentage).compute(degrees);
        return e -> e.weight > threshold;
    }

    private PhraseGraph generatePhraseGraph(Model model, List<Resource> works, PhraseFactory pFac) {
        Stopwatch sw = Stopwatch.createStarted();
        PhraseGraph graph = new PhraseGraphImpl();
        List<Phrase> phrases = works.stream()
                .flatMap(w -> streamObjects(Resource.class, model, w, CMRO.hasAspect))
                .map(pFac).collect(Collectors.toList());
        PhraseGraphExtractor extractor = new PhraseGraphExtractor(graph);
        int blockSize = phrases.size() / 10;
        for (int i = 0; i < phrases.size(); i++) {
            extractor.accept(phrases.get(i));
            if (i % blockSize == 0)
                System.err.printf("Processed %d/%d phrases...\n", i, phrases.size());
        }
        int removed = trimGraph(graph);
        System.err.printf("Completed phrase graph (%s) with %d vertices %s in %s\n",
                getGraphTag(), graph.getVertices().size(),
                removed > 0 ? String.format("(%d edges trimmed)", removed) : "", sw);
        return graph;
    }

    private int trimGraph(PhraseGraph graph) {
        if (!trimSpecializations && !trimGeneralizations) return 0;
        HashSet<ImmutablePair<Phrase, Phrase>> victims = new HashSet<>();

        for (Phrase subj : graph.getVertices()) {
            Set<Phrase> subjGeneralizations = findGeneralizations(subj);
            for (Phrase obj : graph.getSuccessors(subj)) {
                int w = graph.getWeight(subj, obj);
                if (trimGeneralizations) {
                    subjGeneralizations.stream()
                            .filter(g -> !g.equals(subj) && graph.getWeight(g, obj) <= w)
                            .forEach(g -> victims.add(ImmutablePair.of(g, obj)));
                }
                if (trimSpecializations) {
                    boolean shouldTrim = subjGeneralizations.stream()
                            .anyMatch(g -> !g.equals(subj) && graph.getWeight(g, obj) > w);
                    if (shouldTrim)
                        victims.add(ImmutablePair.of(subj, obj));
                }
            }
        }

        victims.forEach(p -> graph.removeAll(p.left, p.right));
        return victims.size();
    }

    private void drawGraph(PhraseGraph graph, String baseName, boolean rootHasNoIn,
                           Predicate<PhraseGraphImpl.Edge> edgeFilter) throws IOException {
        List<PhraseGraphImpl.Edge> edges = graph.stream().filter(edgeFilter)
                .collect(Collectors.toList());
        Set<Phrase> vertices = edges.stream().flatMap(e -> Stream.of(e.from, e.to))
                .collect(Collectors.toSet());
        HashMap<Phrase, Integer> pid = new HashMap<>();
        vertices.forEach(p -> pid.put(p, pid.size()+1));
        double maxWeight = edges.stream().map(e -> e.weight).max(Integer::compareTo).orElse(1);

        File dot = new File(out, baseName + ".dot");
        try (PrintStream ps = new PrintStream(new FileOutputStream(dot))) {
            ps.printf("strict digraph{\n" +
                         "  pack = 4;\n" +
                         "  ranksep = 4;\n");
            vertices.forEach(p -> ps.printf("  p%x [label=\"%s\",shape=\"%s\"];\n",
                    pid.get(p), p.toString(),
                    (rootHasNoIn ? graph.getIncomingEdges(p) : graph.getOutgoingEdges(p))
                            .isEmpty() ? "rect" : "ellipse"));

            edges.forEach(e -> ps.printf("  p%x -> p%x [penwidth=%f,label=\"%d\"," +
                            "color=\"#808080\"];\n", pid.get(e.from), pid.get(e.to),
                    1.0+10*e.weight/maxWeight, e.weight));
            ps.printf("}\n");
        }
        Utils.runDOT(dotCmd, dot, dotFmts, out, baseName, "-Ktwopi");
    }

    private class PhraseGraphExtractor implements Consumer<Phrase> {
        private final PhraseGraph graph;
        PhraseGraphExtractor(PhraseGraph graph) { this.graph = graph; }
        @Override
        public void accept(Phrase p) {
            Stream.of(p.getMain(), p.getCovered(), p.getOf()).flatMap(Set::stream)
                    .forEach(this);
            if (generalizations) {
                Set<Phrase> pGeneralizations = findSubjectGeneralizations(p);
                p.getOf().forEach(o -> findGeneralizations(o)
                        .forEach(og -> pGeneralizations
                                .forEach(pg -> graph.add(pg, og))));
            } else {
                Phrase subject = p.getSubjectAsPhrase();
                p.getOf().forEach(object -> graph.add(subject, object));
            }
        }
    }

    private void analyzeRoots(Model model, List<Resource> works, PhraseFactory pFac)
            throws IOException {
        LinkedHashMap<Phrase, Integer> rootsCounts = countDuplicates(works.stream()
                .flatMap(w -> streamObjects(Resource.class, model, w, CMRO.hasAspect))
                .flatMap(createAspectRootExtractor(false)).map(pFac));
        LinkedHashMap<Phrase, Integer> rootsAndGeneralizationsCounts =
                addGeneralizationsToCounts(countDuplicates(works.stream()
                        .flatMap(w -> streamObjects(Resource.class, model, w, CMRO.hasAspect))
                        .flatMap(createAspectRootExtractor(true))
                        .map(pFac)));

        dumpFrequencies("roots.csv", rootsCounts);
        dumpFrequencies("roots-generalizations.csv", rootsAndGeneralizationsCounts);
    }

    private LinkedHashMap<Phrase, Integer> addGeneralizationsToCounts(
            LinkedHashMap<Phrase, Integer> counts) {
        Multimap<Resource, Phrase> generalizations = HashMultimap.create();
        for (Phrase ad : counts.keySet()) {
            getAllLeafs(ad).stream().flatMap(r -> findGeneralizations(r).stream()).distinct()
                    .forEach(g -> generalizations.put(g, ad));
        }
        LinkedHashMap<Phrase, Integer> expanded = new LinkedHashMap<>();
        counts.forEach(expanded::put);
        for (Resource g : generalizations.keySet()) {
            int count = generalizations.get(g).stream().map(counts::get)
                    .reduce((l, r) -> l + r).orElse(0);
            Phrase ad = expanded.keySet().stream()
                    .filter(other -> other.isSimple() && other.getNamingResource().equals(g))
                    .findAny().orElse(new Phrase(g));
            expanded.put(ad, count);
        }
        return expanded;
    }

    private Set<Resource> getAllLeafs(Phrase ad) {
        Stack<Phrase> stack = new Stack<>();
        stack.push(ad);
        HashSet<Resource> leafs = new HashSet<>();
        while (!stack.isEmpty()) {
            Phrase d = stack.pop();
            if (d.isLeaf())
                leafs.add(d.getLeafResource());
            stack.addAll(d.getMain());
            stack.addAll(d.getCovered());
            stack.addAll(d.getOf());
        }
        return leafs;
    }

    private Set<Resource> findGeneralizations(Resource concrete) {
        Stack<Resource> stack = new Stack<>();
        Set<Resource> visited = new HashSet<>();
        stack.push(concrete);
        while (!stack.isEmpty()) {
            Resource r = stack.pop();
            if (visited.contains(r)) continue;
            visited.add(r);
            streamObjects(Resource.class, r.getModel(), r, RDFS.subClassOf).forEach(stack::push);
            streamObjects(Resource.class, r.getModel(), r, SKOS.broader).forEach(stack::push);
            /* isPartOf is a weak relation, so it is not safe to traverse it */
//            streamObjects(Resource.class, r.loadModel(), r, isPartOf).forEach(stack::push);
        }
        return visited;
    }

    private List<Set<Phrase>> getGeneralizationsOfSimplePhrases(Set<Phrase> simplePhrases) {
        if (simplePhrases.stream().anyMatch(p -> !p.isSimple()))
            return Collections.singletonList(simplePhrases);
        List<ArrayList<Resource>> lists = simplePhrases.stream().map(p -> new ArrayList<>(
                findGeneralizations(p.getNamingResource()))).collect(Collectors.toList());
        assert !lists.isEmpty();
        assert lists.stream().noneMatch(List::isEmpty);
        return Lists.cartesianProduct(lists).stream()
                .map(l -> l.stream().map(Phrase::new).collect(Collectors.toSet()))
                .collect(Collectors.toList());
    }

    private Set<Phrase> findSubjectGeneralizations(Phrase phrase) {
        if (phrase.isLeaf()) {
            return findGeneralizations(phrase.getNamingResource()).stream()
                    .map(Phrase::new).collect(Collectors.toSet());
        }
        List<Set<Phrase>> mainG = getGeneralizationsOfSimplePhrases(phrase.getMain());
        List<Set<Phrase>> coveredG = getGeneralizationsOfSimplePhrases(phrase.getCovered());
        return Lists.cartesianProduct(mainG, coveredG).stream()
                .map(l -> new Phrase(l.get(0), l.get(1),
                        Collections.emptySet(), Collections.emptySet()))
                .collect(Collectors.toSet());
    }

    private Set<Phrase> findGeneralizations(Phrase phrase) {
        if (phrase.isLeaf()) {
            return findGeneralizations(phrase.getNamingResource()).stream()
                    .map(Phrase::new).collect(Collectors.toSet());
        }
        Set<Phrase> result = new HashSet<>(phrase.getSubsumedBy());
        List<Set<Phrase>> mainG = getGeneralizationsOfSimplePhrases(phrase.getMain());
        List<Set<Phrase>> coveredG = getGeneralizationsOfSimplePhrases(phrase.getCovered());
        List<Set<Phrase>> ofG = getGeneralizationsOfSimplePhrases(phrase.getOf());
        Lists.cartesianProduct(mainG, coveredG, ofG).stream()
                .map(l -> new Phrase(l.get(0), l.get(1), l.get(2), Collections.emptySet()))
                .forEach(result::add);
        return result;
    }

    private void dumpFrequencies(String filename,
                                 LinkedHashMap<Phrase, Integer> counts)
            throws IOException {
        counts = sortByCount(counts);
        try (FileWriter writer = new FileWriter(new File(out, filename))) {
            CSVPrinter printer = new CSVPrinter(writer,
                    CSVFormat.DEFAULT.withHeader("item", "count"));
            for (Phrase ad : counts.keySet())
                printer.printRecord(ad, counts.get(ad));
        } catch (IOException e) {
            throw new IOException("Problem writing " + filename, e);
        }
    }

    private <T> LinkedHashMap<T, Integer> countDuplicates(Stream<T> stream) {
        LinkedHashMap<T, Integer> counts = new LinkedHashMap<>();
        stream.forEach(t -> counts.put(t, counts.getOrDefault(t, 0)+1));
        return counts;
    }

    private <T> LinkedHashMap<T, Integer> sortByCount(LinkedHashMap<T, Integer> counts) {
        Multimap<Integer, T> mmap  = HashMultimap.create();
        for (Map.Entry<T, Integer> e : counts.entrySet()) {
            mmap.put(e.getValue(), e.getKey());
        }

        LinkedHashMap<T, Integer> counts2 = new LinkedHashMap<>();
        mmap.keySet().stream().sorted(Comparator.reverseOrder()).flatMap(k -> mmap.get(k).stream())
                .forEach(t -> counts2.put(t, counts.get(t)));
        return counts2;
    }

    private Function<Resource, Stream<Resource>> createAspectRootExtractor(boolean withSubsumedBy) {
        return  (aspect) -> {
            Stack<Resource> stack = new Stack<>();
            Set<Resource> visited = new HashSet<>();
            stack.push(aspect);
            HashSet<Resource> roots = new HashSet<>();
            while (!stack.isEmpty()) {
                Resource r = stack.pop();
                if (visited.contains(r)) continue;
                visited.add(r);
                Model m = r.getModel();
                List<Resource> list = streamObjects(Resource.class, m, r,
                        CMRO.of, CMRO.intersection).collect(Collectors.toList());
                if (list.size() == 0)
                    roots.add(r);
                if (withSubsumedBy)
                    streamObjects(Resource.class, m, r, CMRO.subsumedBy).forEach(list::add);
                stack.addAll(list);
            }
            return roots.stream();
        };
    }

    public static class Builder {
        private File baseDir = null;
        private File out;
        private String dotCmd = "dot";
        private String[] dotFmts = {"pdf"};
        private String[] forwardFrom = {};
        private String[] backwardFrom = {};
        private boolean generalizations = false;
        private boolean trimGeneralizations = false;
        private boolean trimSpecializations = false;

        public Builder withBaseDir(File baseDir) {
            this.baseDir = baseDir;
            return this;
        }

        public Builder withOut(File out) {
            this.out = out;
            return this;
        }

        public Builder withDotCmd(String dotCmd) {
            this.dotCmd = dotCmd;
            return this;
        }

        public Builder withDotFmts(String[] dotFmts) {
            this.dotFmts = dotFmts;
            return this;
        }

        public Builder withForwardFrom(String[] forwardFrom) {
            this.forwardFrom = forwardFrom;
            return this;
        }

        public Builder withBackwardFrom(String[] backwardFrom) {
            this.backwardFrom = backwardFrom;
            return this;
        }

        public Builder withGeneralizations(boolean generalizations) {
            this.generalizations = generalizations;
            return this;
        }

        public Builder withTrimGeneralizations(boolean trimGeneralizations) {
            Preconditions.checkState(!trimGeneralizations || generalizations);
            this.trimGeneralizations = trimGeneralizations;
            return this;
        }

        public Builder withTrimSpecializations(boolean trimSpecializations) {
            Preconditions.checkState(!trimSpecializations || generalizations);
            this.trimSpecializations = trimSpecializations;
            return this;
        }

        public ClassificationsAnalysis build() {
            return new ClassificationsAnalysis(baseDir, out, dotCmd, dotFmts, forwardFrom,
                    backwardFrom, generalizations, trimGeneralizations, trimSpecializations);
        }
    }
}
