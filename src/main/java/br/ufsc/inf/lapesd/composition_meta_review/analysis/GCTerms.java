package br.ufsc.inf.lapesd.composition_meta_review.analysis;

import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GCTerms {
    @Option(name = "--base-dir", usage = "Optional base project dir, will read from this, " +
            "instead of the resources")
    private File baseDir = null;

    @Option(name = "--out-dir", usage = "Output directory for report files")
    private File outDir = null;

    private Loader loader;

    public static void main(String[] args) throws Exception {
        GCTerms app = new GCTerms();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    private void run() throws IOException {
        loader = new Loader(baseDir);
        Model termsModel = loader.loadModel().with(Loader.NamedFile.TERMS_BASE).get();
        Model fullModel = loader.loadModel().with(Loader.NamedFile.TERMS_BASE)
                .with(Loader.NamedFile.CLASSIFICATIONS).get();
        Model clsModel = loader.loadModel().with(Loader.NamedFile.CLASSIFICATIONS).get();

        findDead(termsModel, fullModel, "hard-dead.csv");
        findDead(termsModel, clsModel, "soft-dead.csv");

    }

    private void findDead(Model target, Model source, String fileName) throws IOException {
        Set<Property> ignore = Stream.of(RDF.subject).collect(Collectors.toSet());
        Set<Resource> terms = target.listSubjects().toList().stream()
                .filter(RDFNode::isURIResource).collect(Collectors.toSet());
        StmtIterator it = source.listStatements();
        while (it.hasNext()) {
            Statement s = it.next();
            if (s.getObject().isURIResource() && !ignore.contains(s.getPredicate()))
                terms.remove(s.getResource());
        }

        try (FileWriter fileWriter = new FileWriter(new File(outDir, fileName));
             CSVPrinter printer = new CSVPrinter(fileWriter,
                     CSVFormat.DEFAULT.withHeader("resource"))) {
            for (Resource term : terms) printer.printRecord(resourceToString(term));
        }
    }

    private String resourceToString(Resource resource) {
        if (resource.isAnon()) {
            return "_:" + resource.getId();
        } else {
            return resource.getModel().shortForm(resource.getURI());
        }
    }


}
