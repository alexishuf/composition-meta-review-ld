package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public interface PhraseGraph extends Iterable<PhraseGraph.Edge> {
    class Edge {
        public final Phrase from, to;
        public final int weight;

        public Edge(Phrase from, Phrase to, int weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge edge = (Edge) o;

            if (weight != edge.weight) return false;
            if (from != null ? !from.equals(edge.from) : edge.from != null) return false;
            return to != null ? to.equals(edge.to) : edge.to == null;
        }

        @Override
        public int hashCode() {
            int result = from != null ? from.hashCode() : 0;
            result = 31 * result + (to != null ? to.hashCode() : 0);
            result = 31 * result + weight;
            return result;
        }
    }

    void add(Phrase from, Phrase to);

    int getWeight(Phrase from, Phrase to);

    int removeOne(Phrase from, Phrase to);

    void removeAll(Phrase from, Phrase to);

    Set<Phrase> getSuccessors(Phrase from);

    Set<Phrase> getPredecessors(Phrase from);

    Set<Edge> getOutgoingEdges(Phrase from);

    Set<Edge> getIncomingEdges(Phrase to);

    Set<Phrase> getVertices();

    Iterator<PhraseGraph.Edge> iterator();

    Stream<PhraseGraph.Edge> stream();
}
