package br.ufsc.inf.lapesd.composition_meta_review.generators;

import br.ufsc.inf.lapesd.composition_meta_review.analysis.ClassificationsAnalysis;
import br.ufsc.inf.lapesd.composition_meta_review.analysis.Taxonomies;
import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;

public class GenerateAllResources {
    @Option(name = "--dot-cmd", usage = "Alternative dot command")
    private String dotCmd = "dot";

    @Argument(required = true, usage = "Path to the project's root")
    private File baseDir;

    public static void main(String[] args) throws Exception {
        GenerateAllResources app = new GenerateAllResources();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    private void run() throws Exception {
        new GenerateWorksRDF(
                getLoader().getFile(Loader.NamedFile.CONSOLIDATED_CSV),
                getLoader().getFile(Loader.NamedFile.WORKS_BIB),
                getLoader().getFile(Loader.NamedFile.WORKS)
        ).run();
        new GenerateAdditionalBibliography(
                getLoader().getFile(Loader.NamedFile.BIBLIOGRAPHY),
                false,
                new File[] {}
        ).run();
        new GenerateTerms(getLoader().getFile(Loader.NamedFile.TERMS_BASE),
                getLoader().getFile(Loader.NamedFile.TERMS)
        ).run();
        new Taxonomies(
                baseDir, resourceFile("generated/taxonomies"), dotCmd, null
        ).run();
        ClassificationsAnalysis.Builder cab = new ClassificationsAnalysis.Builder()
                .withBaseDir(baseDir).withDotCmd(dotCmd)
                .withOut(resourceFile("generated/classifications"));
        cab.build().run();
        cab.withGeneralizations(true).build().run();
        cab.withTrimGeneralizations(true ).withTrimSpecializations(false).build().run();
        cab.withTrimGeneralizations(false).withTrimSpecializations(true ).build().run();
        cab.withTrimGeneralizations(true ).withTrimSpecializations(true ).build().run();

        writeAllTurtle();
    }

    private void writeAllTurtle() throws IOException {
        Model m = getLoader().loadModel().withAll(Loader.getAllRDF()).get();
        File outFile = resourceFile("generated/all.ttl");
        try (FileOutputStream outputStream = new FileOutputStream(outFile)) {
            RDFDataMgr.write(outputStream, m, Lang.TURTLE);
        }
    }

    private Loader getLoader() {
        return new Loader(baseDir);
    }

    private File resourceFile(String path) {
        return new File(String.format("%s/src/main/resources/%s", baseDir.getPath(), path));
    }
}
