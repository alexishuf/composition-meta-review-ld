package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import br.ufsc.inf.lapesd.composition_meta_review.vocab.Terms;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.WordNet31;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Phrase {
    private Resource leafResource;
    private Set<Phrase> main;
    private Set<Phrase> covered;
    private Set<Phrase> of;
    private Set<Phrase> subsumedBy;

    public Phrase(Resource leafResource) {
        this.leafResource = leafResource;
        this.main = new HashSet<>();
        this.covered = new HashSet<>();
        this.of = new HashSet<>();
        this.subsumedBy = new HashSet<>();
    }

    public Phrase(Set<Phrase> main, Set<Phrase> covered, Set<Phrase> of, Set<Phrase> subsumedBy) {
        this.leafResource = null;
        this.main = main;
        this.covered = covered;
        this.of = of;
        this.subsumedBy = subsumedBy;
    }

    public Set<Phrase> getCovered() {
        return covered;
    }
    public Set<Phrase> getMain() {
        return main;
    }
    public Set<Phrase> getOf() {
        return of;
    }
    public Set<Phrase> getSubsumedBy() {
        return subsumedBy;
    }
    public Resource getLeafResource() {
        return leafResource;
    }
    public boolean isLeaf() {
        return getLeafResource() != null;
    }

    public boolean isSimple() {
        return isLeaf() || (main.size() == 1 && covered.isEmpty() && of.isEmpty()
                && main.iterator().next().isLeaf());
    }

    public Resource getNamingResource() {
        Preconditions.checkState(isSimple());
        return isLeaf() ? getLeafResource() : main.iterator().next().getLeafResource();
    }

    public Phrase getSubjectAsPhrase() {
        if (isLeaf()) return this;
        return new Phrase(main, covered, Collections.emptySet(), Collections.emptySet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phrase that = (Phrase) o;

        if (leafResource != null ? !leafResource.equals(that.leafResource) : that.leafResource != null)
            return false;
        if (main != null ? !main.equals(that.main) : that.main != null) return false;
        if (covered != null ? !covered.equals(that.covered) : that.covered != null) return false;
        if (of != null ? !of.equals(that.of) : that.of != null) return false;
        return subsumedBy != null ? subsumedBy.equals(that.subsumedBy) : that.subsumedBy == null;
    }

    @Override
    public int hashCode() {
        int result = leafResource != null ? leafResource.hashCode() : 0;
        result = 31 * result + (main != null ? main.hashCode() : 0);
        result = 31 * result + (covered != null ? covered.hashCode() : 0);
        result = 31 * result + (of != null ? of.hashCode() : 0);
        result = 31 * result + (subsumedBy != null ? subsumedBy.hashCode() : 0);
        return result;
    }

    public String toString() {
        if (leafResource != null) {
            String uri = leafResource.getURI();
            if (uri.startsWith(WordNet31.PREFIX)) {
                return "'" + leafResource.getRequiredProperty(RDFS.label)
                        .getLiteral().getLexicalForm() + "'";
            } else if (uri.startsWith(Terms.PREFIX)) {
                return uri.replaceAll(".*#", "");
            } else {
                return leafResource.getModel().shortForm(leafResource.getURI());
            }
        } else {
            boolean complex = main.size() + covered.size() > 1;
            String name = Stream.concat(main.stream().map(Phrase::toString),
                    covered.stream().map(Phrase::toString).map(s -> "~" + s)
            ).reduce((l, r) -> l + "+" + r).orElse("");
            if (!of.isEmpty()) {
                complex = true;
                name += " of " + of.stream().map(Phrase::toString)
                        .reduce((l, r) -> l + "," + r).orElse("");
            }
            if (complex)
                name = "(" + name + ")";
            return name;
        }
    }
}
