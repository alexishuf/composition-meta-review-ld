package br.ufsc.inf.lapesd.composition_meta_review.util;

public class LoaderException extends RuntimeException {
    public LoaderException(String s) {
        super(s);
    }

    public LoaderException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
