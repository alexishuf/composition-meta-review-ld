package br.ufsc.inf.lapesd.composition_meta_review.analysis;

import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import br.ufsc.inf.lapesd.composition_meta_review.util.Utils;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.CMRO;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.commons.io.output.CloseShieldOutputStream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.SKOS;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Taxonomies {
    @Option(name = "--base-dir", usage = "Path to the project root. " +
            "If provided, will be used instead of resources")
    private File baseDir = null;

    @Option(name = "--out-dir", required = true,
            usage = "Base directory where output files will be written")
    private File outDir = null;

    @Option(name = "--dot-cmd", usage = "Alternative dot command")
    private String dotCmd = "dot";

    @Option(name = "--dot-fmt", aliases = {"-T"},
            usage = "Dot output format, e.g. \"png\" for dot's \"-Tpng\"")
    private String[] dotFmts = {"png", "pdf"};

    private Taxonomies() {
    }

    public Taxonomies(File baseDir, File outDir, String dotCmd, String[] dotFmts) {
        this.baseDir = baseDir;
        this.outDir = outDir;
        this.dotCmd = dotCmd == null ? this.dotCmd : dotCmd;
        this.dotFmts = dotFmts == null ? this.dotFmts : dotFmts;
    }

    public static void main (String[] args) throws Exception {
        Taxonomies app = new Taxonomies();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    public void run() throws Exception {
        Loader loader = new Loader(baseDir);
        Model tModel = loader.loadModel().with(Loader.NamedFile.TERMS).get();

        writeTaxonomyFiles(new Forest(SKOS.broader, tModel), "broader");
        writeTaxonomyFiles(new Forest(CMRO.isPartOf, tModel), "isPartOf");
    }

    private void writeTaxonomyFiles(Forest forest, String baseName) throws IOException {
        try (FileOutputStream out = new FileOutputStream(new File(outDir, baseName + ".txt"))) {
            writeTaxonomyDump(forest, out, false);
        }
        try (FileOutputStream out = new FileOutputStream(
                new File(outDir, baseName + "-defs.txt"))) {
            writeTaxonomyDump(forest, out, true);
        }
        File dotFile = new File(outDir, baseName + ".dot");
        try (FileOutputStream out = new FileOutputStream(dotFile)) {
            writeDOT(forest, out);
        }
        Utils.runDOT(dotCmd, dotFile, dotFmts, outDir, baseName);
    }

    private String dotName(Resource resource) {
        Preconditions.checkArgument(resource.isURIResource());
        return resource.getLocalName();
    }

    private void writeDOT(Forest forest, FileOutputStream out) {
        try (PrintStream ps = new PrintStream(out, true)) {
            ps.printf("strict digraph {\n" +
                         "  rankdir = BT;\n");
            StmtIterator it = forest.model.listStatements(null, forest.subClassOf, (RDFNode) null);
            while (it.hasNext()) {
                Statement statement = it.next();
                if (!statement.getObject().isResource()) continue;
                ps.printf("  %s -> %s;\n", dotName(statement.getSubject()),
                        dotName(statement.getResource()));
            }
            ps.printf("}\n");
        }
    }

    private void writeTaxonomyDump(Forest forest, FileOutputStream out, boolean withDefinitions) {
        PrintStream ps = new PrintStream(new CloseShieldOutputStream(out), true);
        for (Tree tree : forest.trees.stream().sorted(Comparator.comparing(t -> t.maxDepth))
                .collect(Collectors.toList())) {
            Stack<ImmutablePair<Resource, Integer>> stack = new Stack<>();
            stack.push(ImmutablePair.of(tree.root, 0));
            while (!stack.isEmpty()) {
                ImmutablePair<Resource, Integer> state = stack.pop();
                String indent = String.join("", Collections.nCopies(state.right, "  "));
                String refs = getReferences(state.left.getProperty(forest.subClassOf));
                refs = refs == null ? "" : " ["+refs+"]";
                ps.printf("%s%s%s", indent, Utils.nodeToString(state.left), refs);
                if (withDefinitions) {
                    Statement statement = state.left.getProperty(SKOS.definition);
                    if (statement == null) {
                        ps.printf(" : /*No definition*/");
                    } else {
                        refs = getReferences(statement);
                        String definition = statement.getObject().asLiteral().getLexicalForm();
                        ps.printf(" : %s%s", definition, refs == null ? "" : " ["+refs+"]");
                    }
                }
                ps.println();
                tree.getChildren(state.left).stream()
                        .map(c -> ImmutablePair.of(c, state.right+1)).forEach(stack::add);
            }
        }
    }

    private String getReferences(Statement statement) {
        if (statement == null) return null;
        String refs = null;
        if (statement.isReified()) {
            refs = Stream.concat(
                    statement.listReifiedStatements().toList().stream()
                            .map(rs -> rs.getProperty(DCTerms.source)),
                    statement.listReifiedStatements().toList().stream()
                            .map(rs -> rs.getProperty(DCTerms.references))
            ).filter(Objects::nonNull)
                    .map(Statement::getObject).map(Utils::nodeToString)
                    .reduce((l, r) -> l + ", " + r).orElse(null);
        }
        return refs;
    }

    static class Tree {
        final Property subClassOf;
        final Resource root;
        final int nodeCount;
        final int maxDepth;
        final Set<Resource> nodes;

        public Tree(Property subClassOf, Resource root, int nodeCount, int maxDepth, Set<Resource> nodes) {
            this.subClassOf = subClassOf;
            this.root = root;
            this.nodeCount = nodeCount;
            this.maxDepth = maxDepth;
            this.nodes = nodes;
        }

        public Set<Resource> getChildren(Resource node) {
            Preconditions.checkArgument(nodes.contains(node),
                    String.format("%s is not a node in the the %s tree.",
                            Utils.nodeToString(node), Utils.nodeToString(root)));
            Set<Resource> set = node.getModel().listSubjectsWithProperty(subClassOf, node).toSet();
            assert set.stream().allMatch(nodes::contains);
            return set;
        }
    }

    class Forest {
        final Property subClassOf;
        final List<Tree> trees;
        final Model model;

        public Forest(Property subClassOf, Model model) {
            this.subClassOf = subClassOf;
            this.model = model;

            SetMultimap<Resource, Resource> nodes = HashMultimap.create();
            StmtIterator it = model.listStatements(null, subClassOf, (RDFNode) null);

            while (it.hasNext()) {
                Statement s = it.next();
                if (!s.getObject().isResource()) continue;
                Set<Resource> roots = getRoots(s.getSubject());
                roots.forEach(r -> nodes.get(r).add(s.getSubject()));
            }
            trees = nodes.keySet().stream().filter(k -> !nodes.get(k).isEmpty())
                    .map(k -> {
                        HashSet<Resource> set = new HashSet<>(nodes.get(k));
                        set.add(k);
                        return new Tree(subClassOf, k, set.size(), maxDepth(k), set);
                    }).collect(Collectors.toList());
        }

        private Set<Resource> getRoots(Resource resource) {
            Stack<Resource> stack = new Stack<>();
            HashSet<Resource> visited = new HashSet<>(), roots = new HashSet<>();
            stack.push(resource);
            while (!stack.isEmpty()) {
                Resource current = stack.pop();
                if (visited.contains(current)) continue;
                visited.add(current);

                Set<Resource> parents = current.getModel()
                        .listObjectsOfProperty(current, subClassOf).toList()
                        .stream().filter(RDFNode::isResource).map(RDFNode::asResource)
                        .collect(Collectors.toSet());
                if (parents.isEmpty())
                    roots.add(current);
                stack.addAll(parents);
            }

            return roots;
        }

        private int maxDepth(Resource root) {
            Queue<ImmutablePair<Resource, Integer>> queue = new LinkedList<>();
            HashSet<Resource> visited = new HashSet<>();
            queue.add(ImmutablePair.of(root, 0));
            int maxDepth = 0;

            while (!queue.isEmpty()) {
                ImmutablePair<Resource, Integer> state = queue.remove();
                Resource r = state.left;
                if (visited.contains(r)) continue;
                visited.add(r);
                maxDepth = Integer.max(maxDepth, state.right);
                r.getModel().listSubjectsWithProperty(subClassOf, r).toList().stream()
                        .map(c -> ImmutablePair.of(c, state.right+1)).forEach(queue::add);
            }

            return maxDepth;
        }
    }
}
