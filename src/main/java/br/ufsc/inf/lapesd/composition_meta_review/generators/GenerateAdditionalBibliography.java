package br.ufsc.inf.lapesd.composition_meta_review.generators;

import bibtex.rdf.bibtex2rdf;
import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import br.ufsc.inf.lapesd.composition_meta_review.util.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateAdditionalBibliography {
    @Option(name = "--skip-rq9")
    private boolean skipRQ9Report = false;

    @Option(name = "--skip-terms")
    private boolean skipTerms = false;

    @Option(name = "--out", required = true, usage = "Output of the RDF with bibliography. " +
            "The syntax is determined from the file extension.")
    private File out;

    @Argument(multiValued = true)
    private File[] additionalBibs = {};

    private static final String NS = "http://lapesd.inf.ufsc.br/composition-meta-review-2017/bibliography#";

    public GenerateAdditionalBibliography() {
    }

    public GenerateAdditionalBibliography(File out, boolean skipRQ9Report, File[] additionalBibs) {
        this.skipRQ9Report = skipRQ9Report;
        this.out = out;
        this.additionalBibs = additionalBibs;
    }

    public static void main(String[] args) throws Exception {
        GenerateAdditionalBibliography app = new GenerateAdditionalBibliography();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    public void run() throws Exception {
        Lang lang = RDFLanguages.filenameToLang(out.getName());
        if (lang == null)
            throw new IllegalArgumentException("Couldn't infer RDF language from " + out.getName());

        List<File> bibs = new ArrayList<>(), tempBibs = new ArrayList<>();
        try {
            if (!skipRQ9Report)
                tempBibs.add(Utils.extractResource("rq9-report/bibliografia.bib"));
            if (!skipTerms)
                tempBibs.add(new Loader().getFile(Loader.NamedFile.TERMS_BIB));
            bibs.addAll(tempBibs);
            Collections.addAll(bibs, additionalBibs);

            Model model = ModelFactory.createDefaultModel();
            model.setNsPrefix("bib", NS);
            for (File bib : bibs) convert(model, bib);

            try (FileOutputStream outputStream = new FileOutputStream(out)) {
                RDFDataMgr.write(outputStream, model, lang);
            }
        } finally {
            tempBibs.forEach(FileUtils::deleteQuietly);
        }
    }

    private void convert(Model model, File bib) throws IOException {
        File file = Files.createTempFile("composition-meta-review", ".ttl").toFile();
        file.deleteOnExit();
        bibtex2rdf.main(new String[] {"-baseURI", NS, "-enc", "UTF-8",
                bib.getAbsolutePath(), file.getAbsolutePath()});
        try (FileInputStream inputStream = new FileInputStream(file)) {
            RDFDataMgr.read(model, inputStream, Lang.RDFXML);
        } finally {
            FileUtils.deleteQuietly(file);
        }
    }
}
