package br.ufsc.inf.lapesd.composition_meta_review.util;

import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RiotException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.*;

public class Loader {
    public File baseDir;

    public enum NamedFile {
        BIBLIOGRAPHY {
            @Override String getResourcePath() { return "generated/bibliography.ttl"; }
        },
        WORKS {
            @Override String getResourcePath() { return "generated/works.ttl"; }
        },
        TERMS {
            @Override String getResourcePath() { return "generated/terms.ttl"; }
        },
        CLASSIFICATIONS {
            @Override String getResourcePath() { return "classifications.ttl"; }
        },
        ONTOLOGY {
            @Override String getResourcePath() { return "ontology.ttl"; }
        },
        WN31_LABELS {
            @Override String getResourcePath() { return "wn31-labels.ttl"; }
        },
        TERMS_BASE {
            @Override String getResourcePath() { return "inputs/terms-base.ttl"; }
        },
        CONSOLIDATED_CSV {
            @Override String getResourcePath() { return "inputs/consolidated.csv"; }
        },
        WORKS_BIB {
            @Override String getResourcePath() { return "inputs/works.bib"; }
        },
        TERMS_BIB {
            @Override String getResourcePath() { return "inputs/terms.bib"; }
        };

        abstract String getResourcePath();
    }

    public Loader(File baseDir) {
        this.baseDir = baseDir;
    }

    public Loader() {
    }

    public File getFile(NamedFile namedFile) throws IOException {
        if (baseDir == null) {
            File file = Files.createTempFile("composition-meta-review", "").toFile();
            ClassLoader loader = getClass().getClassLoader();
            try (InputStream in = loader.getResourceAsStream(namedFile.getResourcePath());
                 FileOutputStream out = new FileOutputStream(file)) {
                IOUtils.copy(in, out);
            }
            return file;
        } else {
            return new File(baseDir.getPath() + "/src/main/resources/"
                    + namedFile.getResourcePath());
        }
    }

    public static List<NamedFile> getAllGenerated() {
        return Arrays.asList(NamedFile.BIBLIOGRAPHY, NamedFile.WORKS, NamedFile.TERMS);
    }
    public static List<NamedFile> getAllHandcrafted() {
        return Arrays.asList(NamedFile.CLASSIFICATIONS, NamedFile.ONTOLOGY, NamedFile.WN31_LABELS);
    }
    public static List<NamedFile> getAllRDF() {
        List<NamedFile> list = new ArrayList<>();
        list.addAll(getAllGenerated());
        list.addAll(getAllHandcrafted());
        return list;
    }

    public InputStream open(NamedFile namedFile) {
        return Loader.class.getClassLoader().getResourceAsStream(namedFile.getResourcePath());
    }

    public ModelLoader loadModel() {
        return loadTo(ModelFactory.createDefaultModel());
    }
    public ModelLoader loadTo(Model model) {
        return new ModelLoader(model);
    }

    public class ModelLoader {
        private Model model;

        public ModelLoader(Model model) {
            this.model = model;
        }

        public ModelLoader with(NamedFile namedFile) throws LoaderException {
            Lang lang = RDFLanguages.filenameToLang(namedFile.getResourcePath());
            try {
                RDFDataMgr.read(model, open(namedFile), lang);
            } catch (RiotException e) {
                throw new LoaderException("Problem parsing " + namedFile, e);
            }
            return this;
        }
        public ModelLoader withAll(Collection<NamedFile> collection) {
            collection.forEach(this::with);
            return this;
        }

        public Model get() {
            return model;
        }
    }


}
