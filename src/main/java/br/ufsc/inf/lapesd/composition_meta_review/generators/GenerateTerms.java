package br.ufsc.inf.lapesd.composition_meta_review.generators;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RiotException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class GenerateTerms {
    @Option(name = "--in", required = true, usage = "The input terms-base file")
    private File inFile;

    @Option(name = "--out", required = true, usage = "The output terms file. The RDF syntax " +
            "is determined from the file extension")
    private File outFile;

    @Option(name = "--skip-skos", usage = "If set, basic SKOS inferences will not be materialized")
    private boolean skipSkos = false;

    @Option(name = "--skos-transitive", usage = "If set *Transitive properties of SKOS will " +
            "be generated")
    private boolean skosTransitive = false;

    public GenerateTerms() {
    }

    public GenerateTerms(File inFile, File outFile) {
        this.inFile = inFile;
        this.outFile = outFile;
    }

    public static void main(String[] args) throws Exception {
        GenerateTerms app = new GenerateTerms();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    public void run() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        try (FileInputStream inputStream = new FileInputStream(inFile)) {
            RDFDataMgr.read(model, inputStream, RDFLanguages.filenameToLang(inFile.getName()));
        } catch (RiotException e) {
            System.err.printf("Problem parsing %s\n", inFile.getPath());
            throw e;
        }

        try (QueryExecution ex = QueryExecutionFactory.create("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "CONSTRUCT { ?s ?p ?o. }\n" +
                "WHERE {\n" +
                "  ?r rdf:subject ?s.\n" +
                "  ?r rdf:predicate ?p.\n" +
                "  ?r rdf:object ?o.\n" +
                "}\n", model)) {
            ex.execConstruct(model);
        }
        if (!skipSkos) {
            try (QueryExecution ex = QueryExecutionFactory.create("PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                    "CONSTRUCT { ?o skos:narrower ?s. }\n" +
                    "WHERE { ?s skos:broader ?o. }\n", model)) {
                ex.execConstruct(model);
            }
            try (QueryExecution ex = QueryExecutionFactory.create("PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                    "CONSTRUCT { ?o skos:broader ?s. }\n" +
                    "WHERE { ?s skos:narrower ?o. }\n", model)) {
                ex.execConstruct(model);
            }
            try (QueryExecution ex = QueryExecutionFactory.create("PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                    "CONSTRUCT { ?o skos:related ?s. }\n" +
                    "WHERE { ?s skos:related ?o. }\n", model)) {
                Model additional = ex.execConstruct();
                model.add(additional);
            }
        }
        if (skosTransitive) {
            throw new UnsupportedOperationException("This feature is not implemented yet!");
        }

        try (FileOutputStream out = new FileOutputStream(outFile)) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
        }
    }
}
