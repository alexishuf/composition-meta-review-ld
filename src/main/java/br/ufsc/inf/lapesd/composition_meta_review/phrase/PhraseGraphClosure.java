package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;

public class PhraseGraphClosure extends PhraseGraphImpl {

    public static PhraseGraphClosure create(Phrase initial,
                                            Function<Phrase, Collection<Edge>> neighbors) {
        PhraseGraphClosure graph = new PhraseGraphClosure();
        Stack<Edge> stack = new Stack<>();
        Set<Edge> visited = new HashSet<>();
        stack.addAll(neighbors.apply(initial));
        while (!stack.isEmpty()) {
            Edge edge = stack.pop();
            if (visited.contains(edge)) continue;
            visited.add(edge);

            Collection<Edge> edges = neighbors.apply(edge.to);
            edges.forEach(e -> {
                graph.add(e.from, e.to);
                graph.setWeight(e.from, e.to, e.weight);
            });
            stack.addAll(edges);
        }

        return graph;
    }

    public static PhraseGraphClosure forward(PhraseGraph graph, Phrase phrase) {
        return create(phrase, graph::getOutgoingEdges);
    }

    public static PhraseGraphClosure backward(PhraseGraph graph, Phrase phrase) {
        return create(phrase, graph::getIncomingEdges);
    }
}
