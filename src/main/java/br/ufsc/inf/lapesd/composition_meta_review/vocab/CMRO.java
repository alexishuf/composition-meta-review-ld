package br.ufsc.inf.lapesd.composition_meta_review.vocab;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class CMRO {
    public static String URI = "http://lapesd.inf.ufsc.br/composition-meta-review-2017/ontology";
    public static String PREFIX = URI + "#";

    public static Resource Work = ResourceFactory.createResource(PREFIX + "Work");
    public static Resource WorkExcludedByDocument = ResourceFactory.createResource(PREFIX + "WorkExcludedByDocument");
    public static Resource BadPublicationTypeWork = ResourceFactory.createResource(PREFIX + "BadPublicationTypeWork");
    public static Resource DuplicateWork = ResourceFactory.createResource(PREFIX + "DuplicateWork");
    public static Resource HasExtendedWork = ResourceFactory.createResource(PREFIX + "HasExtendedWork");
    public static Resource WorkExcludedByTitle = ResourceFactory.createResource(PREFIX + "WorkExcludedByTitle");
    public static Resource WorkExcludedByAbstract = ResourceFactory.createResource(PREFIX + "WorkExcludedByAbstract");
    public static Resource WorkExcludedByContent = ResourceFactory.createResource(PREFIX + "WorkExcludedByContent");
    public static Resource WorkWithOriginal = ResourceFactory.createResource(PREFIX + "WorkWithOriginal");
    public static Resource OffTopicContentWork = ResourceFactory.createResource(PREFIX + "OffTopicContentWork");
    public static Resource WorkWithRQ = ResourceFactory.createResource(PREFIX + "WorkWithRQ");
    public static Resource WorkWithRD = ResourceFactory.createResource(PREFIX + "WorkWithRD");
    public static Resource WorkExcludedByQuality = ResourceFactory.createResource(PREFIX + "WorkExcludedByQuality");
    public static Resource Phrase = ResourceFactory.createResource(PREFIX + "Phrase");
    public static Resource Implicit = ResourceFactory.createResource(PREFIX + "Implicit");

    public static Property id1 = ResourceFactory.createProperty(PREFIX + "id1");
    public static Property id2 = ResourceFactory.createProperty(PREFIX + "id2");
    public static Property id3 = ResourceFactory.createProperty(PREFIX + "id3");
    public static Property hasAspect = ResourceFactory.createProperty(PREFIX + "hasAspect");
    public static Property covers = ResourceFactory.createProperty(PREFIX + "covers");
    public static Property intersection = ResourceFactory.createProperty(PREFIX + "intersection");
    public static Property term = ResourceFactory.createProperty(PREFIX + "term");
    public static Property of = ResourceFactory.createProperty(PREFIX + "of");
    public static Property equivalentPhrase = ResourceFactory.createProperty(PREFIX + "equivalentPhrase");
    public static Property subsumedBy = ResourceFactory.createProperty(PREFIX + "subsumedBy");
    public static Property isPartOf = ResourceFactory.createProperty(PREFIX + "isPartOf");
}
