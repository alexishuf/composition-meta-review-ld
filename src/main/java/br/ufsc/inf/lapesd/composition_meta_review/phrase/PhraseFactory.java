package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import br.ufsc.inf.lapesd.composition_meta_review.vocab.CMRO;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Resource;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static br.ufsc.inf.lapesd.composition_meta_review.util.Utils.streamObjects;

public class PhraseFactory implements Function<Resource, Phrase> {
    Map<Phrase, Phrase> canonPhrases = new HashMap<>();

    @Override
    public Phrase apply(Resource a) {
        Phrase raw = parse(a);
        Phrase canon = canonPhrases.getOrDefault(raw, raw);
        if (canon == raw)
            canonPhrases.put(raw, raw);
        return canon;
    }



    public Phrase parse(Resource a) {
        Set<Phrase> main = new HashSet<>(), covered = new HashSet<>(), of = new HashSet<>(),
                      subsumedBy = new HashSet<>();
        Stream.concat(
                streamObjects(Resource.class, a.getModel(), a, CMRO.intersection),
                streamObjects(Resource.class, a.getModel(), a, CMRO.term)
        ).map(this).forEach(main::add);
        streamObjects(Resource.class, a.getModel(), a, CMRO.covers)
                .map(this).forEach(covered::add);
        streamObjects(Resource.class, a.getModel(), a, CMRO.of)
                .map(this).forEach(of::add);
        streamObjects(Resource.class, a.getModel(), a, CMRO.subsumedBy)
                .map(this).forEach(subsumedBy::add);
        if (main.isEmpty() && covered.isEmpty() && of.isEmpty()) {
            Preconditions.checkArgument(a.isURIResource());
            return new Phrase(a);
        }

        Preconditions.checkArgument(main.size() + covered.size() > 0, "Phrase without subject");
        return new Phrase(main, covered, of, subsumedBy);
    }
}
