package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PhraseGraphImpl implements PhraseGraph {
    private HashMap<ImmutablePair<Phrase, Phrase>, Integer> weights = new HashMap<>();
    private SetMultimap<Phrase, Phrase> successors = HashMultimap.create();
    private SetMultimap<Phrase, Phrase> predecessors = HashMultimap.create();
    private Set<Phrase> vertices = new HashSet<>();

    @Override
    public void add(Phrase from, Phrase to) {
        successors.get(from).add(to);
        predecessors.get(to).add(from);
        vertices.add(from);
        vertices.add(to);
        weights.put(ImmutablePair.of(from, to), getWeight(from, to)+1);
    }

    @Override
    public int getWeight(Phrase from, Phrase to) {
        return weights.getOrDefault(ImmutablePair.of(from, to), 0);
    }

    public void setWeight(Phrase from, Phrase to, int weight) {
        Preconditions.checkState(getWeight(from, to) > 0);
        Preconditions.checkArgument(weight > 0);
        weights.put(ImmutablePair.of(from, to), weight);
    }

    @Override
    public int removeOne(Phrase from, Phrase to) {
        int count = getWeight(from, to);
        if (count <= 1) removeAll(from, to);
        else weights.put(ImmutablePair.of(from, to), count - 1);
        return count;
    }

    @Override
    public void removeAll(Phrase from, Phrase to) {
        weights.remove(ImmutablePair.of(from, to));
        successors.get(from).remove(to);
        predecessors.get(to).remove(from);

        if (successors.get(from).isEmpty() && predecessors.get(from).isEmpty())
            vertices.remove(from);
        if (successors.get(to).isEmpty() && predecessors.get(to).isEmpty())
            vertices.remove(to);
    }

    @Override
    public Set<Phrase> getSuccessors(Phrase from) {
        return Collections.unmodifiableSet(successors.get(from));
    }
    @Override
    public Set<Phrase> getPredecessors(Phrase from) {
        return Collections.unmodifiableSet(predecessors.get(from));
    }

    @Override
    public Set<Edge> getOutgoingEdges(Phrase from) {
        return getSuccessors(from).stream().map(to -> new Edge(from, to, getWeight(from, to)))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Edge> getIncomingEdges(Phrase to) {
        return getPredecessors(to).stream().map(from -> new Edge(from, to, getWeight(from, to)))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Phrase> getVertices() {
        return Collections.unmodifiableSet(vertices);
    }

    @Override
    public Iterator<Edge> iterator() {
        return weights.entrySet().stream()
                .map(e -> new Edge(e.getKey().left, e.getKey().right, e.getValue())).iterator();
    }

    @Override
    public Stream<Edge> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}
