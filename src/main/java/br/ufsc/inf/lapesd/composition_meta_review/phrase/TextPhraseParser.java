package br.ufsc.inf.lapesd.composition_meta_review.phrase;

import br.ufsc.inf.lapesd.composition_meta_review.antlr4.PhraseLexer;
import br.ufsc.inf.lapesd.composition_meta_review.antlr4.PhraseParser;
import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.Terms;
import com.google.common.base.Preconditions;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDFS;

import java.util.*;
import java.util.stream.Collectors;

public class TextPhraseParser {
    private final PhraseFactory phraseFactory;
    private final Model vocab;

    public TextPhraseParser(PhraseFactory phraseFactory, Loader loader) {
        this.phraseFactory = phraseFactory;
        this.vocab = loader.loadModel().with(Loader.NamedFile.WN31_LABELS)
                .with(Loader.NamedFile.TERMS).get();
    }


    public Phrase parse(String text) {
        CodePointCharStream stream = CharStreams.fromString(text);
        PhraseLexer lexer = new PhraseLexer(stream);
        PhraseParser parser = new PhraseParser(new CommonTokenStream(lexer));
        Listener listener = new Listener();
        ParseTreeWalker.DEFAULT.walk(listener, parser.phrase());
        return listener.getPhrase();
    }

    private class Listener extends br.ufsc.inf.lapesd.composition_meta_review.antlr4.PhraseBaseListener {
        private Stack<State> stack = new Stack<>();

        private class State {
            private Set<Phrase> main = new HashSet<>();
            private Set<Phrase> covers = new HashSet<>();
            private Set<Phrase> of = new HashSet<>();
        }

        private Phrase result = null;

        @Override
        public void enterPhrase(PhraseParser.PhraseContext ctx) {
            stack.push(new State());
        }

        public void exitPhrase(PhraseParser.PhraseContext ctx) {
            Phrase phrase;
            State s = stack.pop();
            if (s.main.size() == 1 && s.covers.isEmpty() && s.of.isEmpty())
                phrase = s.main.iterator().next();
            else
                phrase = new Phrase(s.main, s.covers, s.of, Collections.emptySet());

            if (stack.isEmpty()) {
                result = phrase;
            } else {
                boolean isSubj = ctx.getParent() instanceof PhraseParser.AtomContext;
                if (!isSubj) {
                    assert ctx.getParent() instanceof PhraseParser.PhraseContext;
                    PhraseParser.PhraseContext p = (PhraseParser.PhraseContext) ctx.getParent();
                    isSubj = p.phrase(0) == ctx;
                }
                (isSubj ? stack.peek().main : stack.peek().of).add(phrase);
            }
        }

        @Override
        public void exitTerm(PhraseParser.TermContext ctx) {
            String uri = Terms.PREFIX + ctx.TERM();
            Phrase phrase = phraseFactory.apply(vocab.createResource(uri));
            (ctx.getParent() instanceof PhraseParser.CoverContext ?
                    stack.peek().covers : stack.peek().main).add(phrase);
        }

        @Override
        public void exitWnTerm(PhraseParser.WnTermContext ctx) {
            String text = ctx.TERM().getText();
            Phrase phrase;
            try (QueryExecution ex = QueryExecutionFactory.create(
                    "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                            "SELECT ?subj WHERE{\n" +
                            "  ?subj rdfs:label ?l FILTER(str(?l) = \"" + text + "\").\n" +
                            "}", vocab)) {
                phrase = phraseFactory.apply(ex.execSelect().next().get("subj").asResource());
            }
            (ctx.getParent() instanceof PhraseParser.CoverContext ?
                    stack.peek().covers : stack.peek().main).add(phrase);
        }

        public Phrase getPhrase() {
            return compress(result);
        }

        private Phrase compress(Phrase p) {
            if (p.isLeaf()) {
                return p;
            } else {
                p = new Phrase(
                        p.getMain().stream().map(this::compress).collect(Collectors.toSet()),
                        p.getCovered().stream().map(this::compress).collect(Collectors.toSet()),
                        p.getOf().stream().map(this::compress).collect(Collectors.toSet()),
                        p.getSubsumedBy().stream().map(this::compress).collect(Collectors.toSet()));
            }
            Set<Phrase> selected = p.getMain().stream()
                    .filter(c -> !c.isLeaf() && c.getOf().isEmpty() && c.getSubsumedBy().isEmpty())
                    .collect(Collectors.toSet());
            Phrase finalP = p;
            selected.forEach(s -> finalP.getMain().addAll(s.getMain()));
            selected.forEach(s -> finalP.getCovered().addAll(s.getCovered()));
            p.getMain().removeAll(selected);
            return p;
        }
    }
}
