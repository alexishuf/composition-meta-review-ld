package br.ufsc.inf.lapesd.composition_meta_review.util;

import br.ufsc.inf.lapesd.composition_meta_review.analysis.ClassificationsAnalysis;
import br.ufsc.inf.lapesd.composition_meta_review.generators.GenerateAdditionalBibliography;
import org.apache.commons.collections4.iterators.TransformIterator;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Utils {
    public static File extractResource(String resourcePath) throws IOException {
        InputStream in = GenerateAdditionalBibliography.class.getClassLoader().getResourceAsStream(resourcePath);
        File file = Files.createTempFile("composition-meta-review", "").toFile();
        file.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(file)) {
            IOUtils.copy(in, out);
        }
        return file;
    }

    public static String nodeToString(RDFNode node) {
        if (node.isResource()) {
            Resource resource = node.asResource();
            if (resource.isURIResource() && resource.getModel() != null)
                return resource.getModel().shortForm(resource.getURI());
            else if (resource.isAnon())
                return "_:" + resource.getId();
        }
        return node.toString();
    }

    public static <R extends RDFNode> Stream<R> streamObjects(Class<R> rClass, Model m,
                                                              Resource subject, Property property) {
        StmtIterator it = m.listStatements(subject, property, (RDFNode) null);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                new TransformIterator<>(it, s -> s.getObject().as(rClass)), Spliterator.NONNULL),
                false);
    }

    public static <R extends RDFNode> Stream<R> streamObjects(Class<R> rClass, Model m,
                                                              Resource subject, Property property,
                                                              Property... properties) {
        Stream<R> stream = streamObjects(rClass, m, subject, property);
        for (Property property1 : properties)
            stream = Stream.concat(stream, streamObjects(rClass, m, subject, property1));
        return stream;
    }

    public static void runDOT(String dotCmd, File dotFile, String[] outFormats, File outDir,
                              String outBaseName, String... args)
            throws IOException {
        for (String fmt : outFormats)
            runDOT(dotCmd, dotFile, fmt, new File(outDir, outBaseName + "." + fmt), args);
    }

    public static void runDOT(String dotCmd, File dotFile, String dotFmt, File outFile,
                              String... additionalArgs)
            throws IOException {
        ArrayList<String> args = new ArrayList<>(Arrays.asList(dotCmd, "-T" + dotFmt,
                dotFile.getAbsolutePath()));
        Collections.addAll(args, additionalArgs);
        Process dot = new ProcessBuilder(args).redirectOutput(outFile)
                .redirectError(ProcessBuilder.Redirect.INHERIT).start();
        int code;
        try {
            code = dot.waitFor();
        } catch (InterruptedException e) {
            dot.destroyForcibly();
            throw new IOException("interrupted while waiting dot ");
        }
        if (code != 0)
            throw new IOException("dot exited with non-zero code " + code);
    }
}
