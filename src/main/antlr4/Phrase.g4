grammar Phrase;
@header {
    package br.ufsc.inf.lapesd.composition_meta_review.antlr4;
}

phrase    : <assoc=right> phrase 'of' phrase | subj ;
subj      : atom ( '+' atom )* ;
atom      : term | wnTerm | cover | '(' phrase ')' ;
cover     : '~' ( term | wnTerm ) ;
term      : TERM;
wnTerm    : '\'' TERM '\'';
WS        : [ \t\r\n]+ -> skip ;
OF        : 'of' ;
TERM      : [a-zA-Z0-9]+ ;
