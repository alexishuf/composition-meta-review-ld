package br.ufsc.inf.lapesd.composition_meta_review;

import br.ufsc.inf.lapesd.composition_meta_review.phrase.Phrase;
import br.ufsc.inf.lapesd.composition_meta_review.phrase.PhraseFactory;
import br.ufsc.inf.lapesd.composition_meta_review.phrase.TextPhraseParser;
import br.ufsc.inf.lapesd.composition_meta_review.util.Loader;
import br.ufsc.inf.lapesd.composition_meta_review.vocab.Terms;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TextPhraseParserTests {

    @DataProvider
    public static Object[][] parseData() {
        Model m = ModelFactory.createDefaultModel();
        final String PREFIX = "http://lapesd.inf.ufsc.br/composition-meta-review-2017/test#";
        RDFDataMgr.read(m, Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("text-phrases-test.ttl"), Lang.TURTLE);
        new Loader().loadTo(m).with(Loader.NamedFile.TERMS)
                .with(Loader.NamedFile.WN31_LABELS).get();
        return new Object[][] {
                {"ServiceCompositionArtifact",
                        m.createResource(Terms.PREFIX + "ServiceCompositionArtifact")},
                {"ServiceDescription+ServiceDiscovery", m.createResource(PREFIX + "2")},
                {"ServiceDescription+~ServiceDiscovery", m.createResource(PREFIX + "3")},
                {"ServiceDescription+~ServiceDiscovery of ServiceCompositionArtifact",
                        m.createResource(PREFIX + "4")},
                {"'moment' of ServiceCompositionProcess", m.createResource(PREFIX + "5")},
                {"NonFunctionalRequirement of (Language of ServiceDescription)",
                        m.createResource(PREFIX + "6")},
                {"'moment' of (ServiceDescription+~ServiceDiscovery of ServiceCompositionArtifact)",
                        m.createResource(PREFIX + "7")},
        };
    }

    @Test(dataProvider = "parseData")
    public void testParse(String text, Resource phraseResource) {
        Phrase expected = new PhraseFactory().apply(phraseResource);
        TextPhraseParser parser = new TextPhraseParser(new PhraseFactory(), new Loader());
        Phrase phrase;
        try {
            phrase = parser.parse(text);
        } catch (Throwable t) {
            Assert.fail("parse threw an error", t);
            return;
        }
        Assert.assertNotNull(phrase);
        Assert.assertEquals(phrase, expected);
    }
}
